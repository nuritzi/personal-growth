# About

This project tracks Nuritzi's personal growth and career plan.

## Relevant Links
 * [Job Description](https://about.gitlab.com/job-families/marketing/open-source-program-manager/)
 * [Creating a Promotion or Compensation Change Document](https://about.gitlab.com/handbook/people-group/promotions-transfers/#creating-a-promotion-or-compensation-change-document)
 * [Accomplishments Tracker](https://docs.google.com/document/d/16NQRVRt49ztkDjHEUBuPqDvUnWYFoTutLYYOXFfN7bk/edit)
 * [Career Mapping and Development](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#career-mapping-and-development)
 * [Competencies](https://about.gitlab.com/handbook/competencies/)
